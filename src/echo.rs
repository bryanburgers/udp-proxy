#[macro_use]
extern crate futures;
extern crate tokio;

use std::io;
use std::io::{stdout, Write};
use std::net::SocketAddr;
use futures::Future;
use futures::Poll;
use tokio::net::UdpSocket;
use tokio::prelude::*;

use std::sync::Arc;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::thread;

fn main() {
    let addr = std::env::args().nth(1).unwrap_or("0.0.0.0:8000".into()).parse().unwrap();

    let counter = Arc::new(AtomicUsize::new(0));

    let socket = UdpSocket::bind(&addr).unwrap();

    println!("Listening on: {}", socket.local_addr().unwrap());

    counter_thread(counter.clone());

    let server = Server {
        counter: counter.clone(),
        socket: socket,
        buf: vec![0; 1024],
        to_send: None,
    }.map_err(|e| println!("error = {:?}", e));

    tokio::run(server)
}

fn counter_thread(counter: Arc<AtomicUsize>) {
    thread::spawn(move || {
        let mut stdout = stdout();
        loop {
            thread::sleep(std::time::Duration::from_millis(1000));
            let messages = counter.swap(0, Ordering::Relaxed);
            print!("\x1B[1G\x1B[0K{} messages", messages);
            stdout.flush();
        }
    });
}

struct Server {
    counter: Arc<AtomicUsize>,
    socket: UdpSocket,
    buf: Vec<u8>,
    to_send: Option<(usize, SocketAddr)>,
}

impl Future for Server {
    type Item = ();
    type Error = io::Error;
    
    fn poll(&mut self) -> Poll<(), io::Error> {
        loop {
            if let Some((size, peer)) = self.to_send {
                let amt = try_ready!(self.socket.poll_send_to(&self.buf[..size], &peer));
                self.counter.fetch_add(1, Ordering::Relaxed);
                self.to_send = None;
            }

            self.to_send = Some(try_ready!(self.socket.poll_recv_from(&mut self.buf)));
        }
    }
}
