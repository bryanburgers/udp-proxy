extern crate bytes;
#[macro_use]
extern crate futures;
extern crate tokio;
extern crate tokio_io;

use std::io;
use std::net::SocketAddr;
use bytes::{Bytes, BytesMut, Buf};
use futures::{Future, Poll};
use futures::sync::mpsc;
use tokio::net::{UdpFramed, UdpSocket};
use tokio::prelude::*;
use futures::{Stream, Sink};
use tokio_io::codec::BytesCodec;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

// Primary: The UDP socket that this application is listening on
// Forwarder: The UDP socket created by this application to handle responses
// Target: The address of the server that we're sending data to
// Client: The address of the UDP client that sent us messages

fn main() {
    let addr_listen = std::env::args()
        .nth(1)
        .unwrap_or("0.0.0.0:8000".into())
        .parse()
        .unwrap();

    let target_addresses: Vec<SocketAddr> = std::env::args()
        .skip(2)
        .map(|s| s.parse().unwrap())
        .collect();

    let socket = UdpSocket::bind(&addr_listen).unwrap();
    println!("Listening on: {}", socket.local_addr().unwrap());

    let framed = UdpFramed::new(socket, BytesCodec::new());
    let (sink, stream) = framed.split();

    let (to_primary, from_forwarder) = mpsc::unbounded();

    /*
    let f1 = from_forwarder
        .map_err(|_| std::io::Error::new(std::io::ErrorKind::Other, "Oh no!"))
        .forward(sink);
        */
    let f1 = from_forwarder
        .forward(sink.sink_map_err(|_| ()));

    // let f1 = from_forwarder.forward(Sink::sink_map_error(sink, |err| { println!("Ahh! {}", err); }));
    let state = Arc::new(Mutex::new(State::new(to_primary)));

    let f2 = stream.for_each(move |(bytes, address)| {
        process(address, &target_addresses, bytes.freeze(), state.clone());
        Ok(())
    });

    tokio::run(futures::future::lazy(|| {
        tokio::spawn(f1.into_future().map(|_| ()));
        tokio::spawn(f2.into_future().map(|_| ()).map_err(|err| { println!("Ahh4 {}", err); }));

        futures::future::ok(())
    }));
}

fn process(
    client_address: SocketAddr,
    target_addresses: &Vec<SocketAddr>,
    data: Bytes,
    state: Arc<Mutex<State>>,
) {
    if !state.lock().unwrap().map.contains_key(&client_address) {
        // Create a channel for this forwarder
        let (to_forwarder, from_primary) = mpsc::unbounded();

        let to_primary = state.lock().unwrap().to_primary.clone();

        let addr = "0.0.0.0:0".parse().unwrap();
        let socket = UdpSocket::bind(&addr).unwrap();
        let framed = UdpFramed::new(socket, BytesCodec::new());
        let (sink, stream) = framed.split();

        let mut locked_state = state.lock().unwrap();
        let s = locked_state.sender;
        locked_state.sender = (locked_state.sender + 1) % target_addresses.len();

        let target_address = target_addresses[s].clone();
        let f1 = from_primary
            .map(move |bytes| (bytes, target_address))
            // .map(|x| { println!("from:primary, to:forwarder {:?}", x); x })
            .map_err(|_| std::io::Error::new(std::io::ErrorKind::Other, "Oh no1"))
            .forward(sink);
        
        let f2 = stream
            .map(move |(bytes, _)| (bytes.freeze(), client_address))
            .map_err(|err| println!("Ahh5 {}", err))
            .forward(to_primary.sink_map_err(|_| ()));

        tokio::spawn(f1.into_future().map(|_| ()).map_err(|err| { println!("Ahh1 {}", err); }));
        tokio::spawn(f2.into_future().map(|_| ()));

        locked_state
            .map
            .insert(client_address, to_forwarder);
    }

    match state.lock().unwrap().map.get(&client_address) {
        None => unreachable!(),
        Some(to_forwarder) => {
            to_forwarder.unbounded_send(data).unwrap();
        }
    }
}

struct State {
    sender: usize,
    to_primary: mpsc::UnboundedSender<(Bytes, SocketAddr)>,
    map: HashMap<SocketAddr, mpsc::UnboundedSender<Bytes>>,
}

impl State {
    fn new(to_primary: mpsc::UnboundedSender<(Bytes, SocketAddr)>) -> Self {
        State {
            sender: 0,
            to_primary: to_primary,
            map: HashMap::new(),
        }
    }
}
