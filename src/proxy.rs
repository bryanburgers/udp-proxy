#[macro_use]
extern crate futures;
extern crate tokio;

use std::io;
use std::net::SocketAddr;
use futures::{Future, Poll};
use futures::sync::mpsc;
use tokio::net::UdpSocket;
use tokio::prelude::*;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

// Primary: The UDP socket that this application is listening on
// Forwarder: The UDP socket created by this application to handle responses
// Target: The address of the server that we're sending data to
// Client: The address of the UDP client that sent us messages

fn main() {
    let addr_listen = std::env::args()
        .nth(1)
        .unwrap_or("0.0.0.0:8000".into())
        .parse()
        .unwrap();
    let addr_send: SocketAddr = std::env::args()
        .nth(2)
        .unwrap_or("0.0.0.0:8001".into())
        .parse()
        .unwrap();

    let socket = UdpSocket::bind(&addr_listen).unwrap();

    println!("Listening on: {}", socket.local_addr().unwrap());

    let (to_primary, from_forwarder) = mpsc::unbounded();
    let state = Arc::new(Mutex::new(State::new(to_primary)));
    let primary = Primary {
        socket: socket,
        from_forwarder: from_forwarder,
        state: state.clone(),
        target_address: addr_send.clone(),
        buf: None,
    };

    tokio::run(primary.map_err(|e| println!("primary error = {:?}", e)))
}

fn process(
    client_address: SocketAddr,
    target_address: SocketAddr,
    data: Vec<u8>,
    state: Arc<Mutex<State>>,
) {
    if !state.lock().unwrap().map.contains_key(&client_address) {
        let forwarder = Forwarder::new(client_address, target_address, state.clone());
        tokio::spawn(forwarder.map_err(|e| println!("forwarder error = {:?}", e)));
    }

    match state.lock().unwrap().map.get(&client_address) {
        None => unreachable!(),
        Some(tx) => {
            tx.unbounded_send(data).unwrap();
        }
    }
}

struct State {
    to_primary: mpsc::UnboundedSender<(SocketAddr, Vec<u8>)>,
    map: HashMap<SocketAddr, mpsc::UnboundedSender<Vec<u8>>>,
}

impl State {
    fn new(to_primary: mpsc::UnboundedSender<(SocketAddr, Vec<u8>)>) -> Self {
        State {
            to_primary: to_primary,
            map: HashMap::new(),
        }
    }
}

struct Primary {
    from_forwarder: mpsc::UnboundedReceiver<(SocketAddr, Vec<u8>)>,
    socket: UdpSocket,
    state: Arc<Mutex<State>>,
    target_address: SocketAddr,
    buf: Option<(SocketAddr, Vec<u8>)>,
}

impl Future for Primary {
    type Item = ();
    type Error = io::Error;

    fn poll(&mut self) -> Poll<(), io::Error> {
        if let Some((ref client_address, ref v)) = self.buf {
            try_ready!(self.socket.poll_send_to(&v, &client_address));
        }
        self.buf = None;

        // Receive all messages from rx that we need to send back to the client.
        loop {
            // Polling an `UnboundedReceiver` cannot fail, so `unwrap` here is safe.
            match self.from_forwarder.poll().unwrap() {
                Async::Ready(Some((client_address, v))) => {
                    match self.socket.poll_send_to(&v, &client_address) {
                        Ok(Async::NotReady) => {
                            // Save the messages for the future!
                            self.buf = Some((client_address, v));

                            // TODO: Do we need to break or return here?
                        }
                        Ok(Async::Ready(_)) => {}
                        Err(err) => {
                            return Err(err);
                        }
                    };
                }
                _ => break,
            }
        }

        // Receive all messages from the socket that we need to send to a forwarder.
        loop {
            let mut vec = vec![0; 1024];
            match self.socket.poll_recv_from(&mut vec) {
                Err(err) => {
                    return Err(err);
                }
                Ok(Async::NotReady) => break,
                Ok(Async::Ready((size, client_address))) => {
                    process(
                        client_address,
                        self.target_address.clone(),
                        vec[..size].to_vec(),
                        self.state.clone(),
                    );
                }
            }
        }

        Ok(Async::NotReady)
    }
}

struct Forwarder {
    from_primary: mpsc::UnboundedReceiver<Vec<u8>>,
    forwarder_socket: UdpSocket,
    client_address: SocketAddr,
    target_address: SocketAddr,
    state: Arc<Mutex<State>>,
    buf: Option<Vec<u8>>,
}

impl Forwarder {
    fn new(
        client_address: SocketAddr,
        target_address: SocketAddr,
        state: Arc<Mutex<State>>,
    ) -> Forwarder {
        // Create a channel for this forwarder
        let (to_forwarder, from_primary) = mpsc::unbounded();

        let addr = "0.0.0.0:0".parse().unwrap();
        let socket = UdpSocket::bind(&addr).unwrap();

        state
            .lock()
            .unwrap()
            .map
            .insert(client_address, to_forwarder);

        Forwarder {
            from_primary: from_primary,
            forwarder_socket: socket,
            client_address: client_address,
            target_address: target_address,
            state: state,
            buf: None,
        }
    }
}

impl Drop for Forwarder {
    fn drop(&mut self) {
        self.state.lock().unwrap().map.remove(&self.client_address);
    }
}

impl Future for Forwarder {
    type Item = ();
    type Error = io::Error;

    fn poll(&mut self) -> Poll<(), io::Error> {
        if let Some(ref v) = self.buf {
            try_ready!(self.forwarder_socket.poll_send_to(v, &self.target_address));
        }
        self.buf = None;

        // Receive all messages that we need to send to the target.
        loop {
            // Polling an `UnboundedReceiver` cannot fail, so `unwrap` here is safe.
            match self.from_primary.poll().unwrap() {
                Async::Ready(Some(v)) => {
                    match self.forwarder_socket.poll_send_to(&v, &self.target_address) {
                        Ok(Async::NotReady) => {
                            // Save the messages for the future!
                            self.buf = Some(v);
                        }
                        Ok(Async::Ready(_)) => {}
                        Err(err) => {
                            return Err(err);
                        }
                    };
                }
                _ => break,
            }
        }

        let mut vec = vec![0; 1024];
        while let Async::Ready((size, _addr)) = self.forwarder_socket.poll_recv_from(&mut vec)? {
            // The send only fails if the rx half has been dropped. However, this is impossible as
            // the `tx` half will be removed from the map before the `rx` is dropped.
            self.state
                .lock()
                .unwrap()
                .to_primary
                .unbounded_send((self.client_address.clone(), vec[..size].to_vec()))
                .unwrap();
        }

        // As always, it is important to not just return `NotReady` without ensuring an inner
        // future also returned `NotReady`.
        //
        // We know we got a `NotReady` from either `self.from_primary` or `self.forwarder_socket`,
        // so the contract is respected.
        Ok(Async::NotReady)
    }
}
