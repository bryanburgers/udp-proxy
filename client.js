'use strict'

const dgram = require('dgram')


if (process.argv.length < 3) {
    console.log('Usage: node client <address> [clients=1] [hz=10]')
    process.exit(1)
}

const [address, portString] = process.argv[2].split(':')
const port = parseInt(portString, 10)

const sockets = parseInt(process.argv[3], 10) || 1
const hz = parseFloat(process.argv[4], 10) || 10
const timeout = 1000 / hz

let sent = 0
let delays = []
function onMessage(buffer, rinfo) {
    const id = buffer.readUInt32LE(0)
    const seconds = buffer.readUInt32LE(4)
    const nanoseconds = buffer.readUInt32LE(8)

    const result = process.hrtime([seconds, nanoseconds])
    delays.push(result[0] * 1e9 + result[1])
}

for (let id = 1; id <= sockets; id++) { 
    const socket = dgram.createSocket({
        type: 'udp4',
    })

    setInterval(() => {
        sent++
        const [seconds, nanoseconds] = process.hrtime()
        const buffer = Buffer.allocUnsafe(12)
        buffer.writeUInt32LE(id, 0, true)
        buffer.writeUInt32LE(seconds, 4, true)
        buffer.writeUInt32LE(nanoseconds, 8, true)
        socket.send(buffer, port, address)
    }, timeout)
        
    socket.on('message', onMessage)
}

setInterval(() => {
    const result = Array.from(delays)
    result.sort((a, b) => {
        return a - b
    })
    const length = delays.length
    const medianIndex = Math.floor(length / 2)
    const ninetyFifthIndex = Math.floor(length * 0.95)
    const median = result[medianIndex] / 1e6
    const ninetyFifth = result[ninetyFifthIndex] / 1e6
    
    const txStr = sent.toLocaleString(undefined, {
        style: 'decimal',
    }).padStart(5) + ' tx'
    const rxStr = length.toLocaleString(undefined, {
        style: 'decimal',
    }).padStart(5) + ' rx'
    const rxTxRatio = (length / sent).toLocaleString(undefined, {
        style: 'percent',
        minimumFractionDigts: 1,
        maximumFractionDigts: 1,
    }).padStart(8) + ' rx/tx'
    const medianStr = median.toLocaleString(undefined, {
        style: 'decimal',
        minimumFractionDigits: 5,
        maximumFractionDigits: 5,
    }).padStart(12) + 'ms median'
    const ninetyFifthStr = ninetyFifth.toLocaleString(undefined, {
        style: 'decimal',
        minimumFractionDigits: 5,
        maximumFractionDigits: 5,
    }).padStart(12) + 'ms 95pctl'
    const display = `${rxStr} ${txStr} ${rxTxRatio} / ${medianStr} / ${ninetyFifthStr}`

    process.stdout.write(`\x1B[1G\x1B[0K${display}`)
    delays = []
    sent = 0
}, 1000)
