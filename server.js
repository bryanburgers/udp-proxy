'use strict'

const dgram = require('dgram')

if (process.argv.length < 3) {
    console.log('Usage: node server 0.0.0.0:8000')
    process.exit(1)
}

const [address, port] = process.argv[2].split(':')

const server = dgram.createSocket({
    type: 'udp4',
})
server.bind(parseInt(port, 10), address, () => {
    const address = server.address()
    console.log(`${address.address}:${address.port}`)
})
let messages = 0
server.on('message', (buffer, rinfo) => {
    messages += 1
    server.send(buffer, rinfo.port, rinfo.address)
})

setInterval(() => {
    const display = messages.toLocaleString(undefined, {
        style: 'decimal',
    }).padStart(9) + ' messages/second'

    process.stdout.write(`\x1B[1G\x1B[0K${display}`)
    messages = 0
}, 1000)
