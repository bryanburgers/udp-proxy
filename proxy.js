'use strict'

const dgram = require('dgram')

if (process.argv.length < 3) {
    console.log('Usage: node server 0.0.0.0:8000 0.0.0.0:8001')
    process.exit(1)
}

const map = new Map()

const [address, port] = process.argv[2].split(':')
const [targetAddress, targetPort] = process.argv[3].split(':')

const server = dgram.createSocket({
    type: 'udp4',
})
server.bind(parseInt(port, 10), address, () => {
    const address = server.address()
    console.log(`${address.address}:${address.port}`)
})
server.on('message', (buffer, rinfo) => {
    const key = `${rinfo.address}:${rinfo.port}`
    if (!map.has(key)) {
        const socket = dgram.createSocket({
            type: 'udp4',
        })
        socket.bind(0, '0.0.0.0')
        socket.on('message', buffer => {
            server.send(buffer, rinfo.port, rinfo.address)
        })

        map.set(key, socket)
    }
    const socket = map.get(key)
    socket.send(buffer, targetPort, targetAddress)
})
